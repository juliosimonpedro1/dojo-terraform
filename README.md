# Dojo Server Templating Tools and Server Providing Tools
	
![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white)

[Carreta]

A continuación, se enumerarán los pasos para completar este dojo. 

## Requisitos: 
- Contar con [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) y contar con cuenta en un controlador de versiones remoto como [GitLab](https://about.gitlab.com/) o [Github](https://github.com/)
- Contar con [Docker](https://docs.docker.com/desktop/install/windows-install/) como herramienta de plantilla de servidores.
- Contar con una cuenta de [AWS](https://aws.amazon.com/es/)

## Pasos para la realización del Dojo


1. Abril la Terminal de git y clonar el repositorio donde se enucentra el código del Dojo

    ```git
    git clone https://gitlab.com/juliosimonpedro1/dojo-terraform.git
    ```
 2. Verifique que todas las ramas se encuentran.
    ```git
    git branch -r 
    ```
3. Cambie su repositorio a la rama 
    ```git
    git checkout origin/terraform
    ```
En el repositorio, dentro de la carpeta _dojo_,  encontrará una carpeta llamada _infrastructure_, con 3 archivos _(main.tf, server.tf, variables.tf)_ y afuera de este un archivo llamado _Dockerfile_. 

Posteriormente, será proveído con una llave de acceso, y el secreto de ésta. Con el fin de usarlas como acceso programatico a AWS. 

4. Luego de contar con la llave de acceso y su secreto, abra su terminal para correr comandos de Docker :whale:. Cerciórese de que el Docker Engine está corriendo con el siguiente comando. 

    ```docker
    docker --version
    ```
    - Si no cuenta con el Motor de Docker iniciado, puede iniciarlo con el comando `sudo systemctl start docker`, si usa una distribuciión linux como Ubuntu o Debian . O si tiene Docker Desktop abra la aplicación.
5. Ahora vaya a la carpeta _*Dojo*_ donde se encuentra el archivo llamado Dockerfile y ejecute el siguiente comando. Con el fn de crear una imagen con las especificaciones escritas en el archivo _Dockerfile_, con los argumentos necesarios. 

    ```docker
    docker build -t infra-dojo:latest --build-arg KEY_ID=<Llave de Acceso que se le entregó> --build-arg SECRET_ID=<Secreto de esa llave de Acceso>  --build-arg Author=<Su nombre o alias> .
    ```
    - Recuerde en este comando remplazar los argumentos *_KEY_ID_*, *_SECRET_ID_*, *_Author_*. Con los valores sugeridos dentro los símbolos `<>`. En mi caso, remplazaría el argumento del autor de la siguiente manera `Author=Jusipe`
6. Luego de crear la imagen corra el siguiente comando, con el fin de crear el contenedor donde se aloja la aplicaicón de Terraform. 

    ```docker
    docker run -it --name dojo-infra infra-dojo:latest
    ```
    - Ahora usted se encuentra dentro del contenedor en la carpeta _/infrastructure_. Este contenedor tiene las características para correr el dojo, debido a que ya cuenta con *Terraform* instalado, así como la consola CLI de AWS. El contenedor tiene como SO, _Ubuntu 20.04_
7. Ahora usted va a correr el comando de _Terraform_.

    ```terraform 
    terraform plan
    ```
    - Luego de la ejecución de este comando, usted verá en su terminal la lista de recursos que _Terraform_ va a crear en _AWS_ y las variables que va a arrojar como outputs. 
8. Para implementar y crear estos recursos en la cuenta de AWS. Tendrá que ejecutar el siguiente comando
    ```terraform
    terraform apply
    ```
    - Este comando, le mostrará lo mismo que el anterior, en el cual, mostrará los recursos a desplegar, a borrar, o a modificiar. Pero la diferencia, es que le preguntara si acepta los cambios. Éstos cambios solo se aceptaran si usted escribe en la terminal `yes` luego del prompt `Do you want to perform these actions?`
    - Luego de unos segundos, aparecerá que los recursos fueron aplicados, y hará un resumen de cuantos recursos son nuevos, cuantos de modificación y cuantos destruidos. 
        - Para la respuesta de esta práctica, solo apareceran 2 recursos creados. (El servidor en EC2 y el Security Group)
    - :rotating_light: Es de mucha importancia que copie la IP que le devolvió el comando. Se debe de ver así `public_ip = "34.230.22.164"` :rotating_light: 
    - :warning: No se salga del contenedor, ni vaya a presionar `CTRL + C` Cuando esté dentro de éste. :warning:

En este momento será provisionado con un archivo `.pem`. El cual será utlizado para acceder al servidor por medio de `ssh`, que acaba de crear utilizando Terraform. 

9. Descargue el archivo y vaya a la ubicación de éste mediante la terminal. Si es usuario de Windows, puede utilizar la terminal de git bash, o si es usuario ubuntu puede ir directamente a esta. Ejecutr el siguiente comando, con el fin de otorgarle permisos solo de lectura al dueño del archivo. 

    ```bash
    chmod 400 dojo-terraform.pem  
    ```
10. Posteriormente, para poder entrar al servidor por medio de `ssh`. Será necesario ejecutar el siguiente comando. 

    ```bash
    ssh -i "dojo-terraform.pem" ubuntu@ec2-<La IP de su servidor>.compute-1.amazonaws.com
    ```
    - Donde es necesario que remplace lo que se encuentra dentro de `<>` por la IP obtenida en el punto 8, separada por `-` envés de `.` `34-230-22-164`. 
    - En este caso el comando quedaría de la siguiente manera `ssh -i "dojo-terraform.pem" ubuntu@ec2-34-230-22-164.compute-1.amazonaws.com`
    - Luego de ejecutar el comando. Habrá un prompt que le preguntará `Are you sure you want to continue connecting (yes/no/[fingerprint])?`. A lo que se escribirá `yes`. 

Ahora se encuentra dentro del servidor que levantó mediante Terraform en el paso 8. El cual es una instancia de un recurso de AWS llamado EC2 (Elastic Compute Cloud). Ahora los últimos pasos radican en ejecutar la aplicación web desde el servidor, la cual se encuentra contenida en una imagen de Docker también. 

11. Se debe de Pullear la imagen desde [Docker Hub](https://hub.docker.com/search?q=) con el siguiente comando.

    ```docker
    docker pull juliosimonpedro/dojo-terraform:latest
    ```
12. Posteriormente para levantar el contenedor de la imagen que se descargó. Se debe de correr el siguiente comando
    ```docker
    docker run -p 8501:8501 $(docker images -aq)
    ```
13. Vaya a su navegador y ponga en el lugar de la URL la dirección de la aplicación que sería la IP pública de su servidor, que obtuvo en el `punto 8` y el puerto `8501`. En este caso
    ```
    34.230.22.164:8501
    ```
Ahora puede visualizar en su navegador, la pequeña aplicación que fue preparada para este dojo.

14. Por último, vaya a la terminal donde tiene el contenedor lanzado en el paso 6, es decir; desde donde se ejecutaron todos los comandos de Terraform. Y ejecute el siguiente comando en esa terminal; con el fin de destruir todos los recursos creados anteriormente. 

    ```terraform
    terraform destroy
    ```
    - Al aplicar este comando, aparecerá un promt similar al del paso 8. `Do you really want to destroy all resources?`. Al cual usted ingresará en la terminal `yes`.
